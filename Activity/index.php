<?php require_once './code.php' ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02 Activity</title>
</head>
<body>

	<h1>Loops</h1>

		<?php divisibleByFive(); ?>

	<hr/>

	<h1>Array</h1>

		<pre><?php print_r($students); ?></pre>

		<?php array_push($students, 'John Smith') ?>
		<pre><?php print_r($students); ?></pre>

		<?php array_push($students, 'Jane Smith') ?>
		<pre><?php print_r($students); ?></pre>

		<?php array_shift($students) ?>
		<pre><?php print_r($students); ?></pre>



</body>
</html>